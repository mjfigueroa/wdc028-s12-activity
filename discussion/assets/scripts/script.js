function determineTyphoonIntensity(windSpeed){
	// console.log(windSpeed);
	
	// if statement

	/*
	if (condition) {
		block of code goes here
	}
	*/

	/*
	if (condition) {
		block of code goes here
	}else {
		another block of code goes here
	}
	*/

	/*if (condition) {
		block of code goes here
	}else if (condition) {
		block of code goes here
	}else {
		another block of code goes here
	}*/

	if (windSpeed < 30) {
		// console.log('hello');
		return 'Not a typhoon yet';
	}else if (windSpeed <= 61) {
		return 'Tropical depression detected';
	}else if (windSpeed >= 62 && windSpeed <= 88) { //windSpeed <= 88
		return 'Tropical storm detected';
	}else if (windSpeed <= 117) {
		return 'Severe tropical storm detected';
	}else {
		return 'Typhoon detected';
	}
};


// ternary operator
function isUnderAge(age) {
	// return (condition) ? true : false;
	return (age < 18) ? 'underage' : 'overage';
	/*
	if (age < 18){
	return true
	} else {
	return false
	}
	*/
}


// switch statement
function determineComputerUser(computerNumber) {
	switch (computerNumber) {
		case 1:
			return 'Bill Gates';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		default:
			return computerNumber + ' is out of bounds';
			break;
	}
}

// try-catch-finally
function showIntensityAlert(windSpeed) {
	try {
		// attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (err) {
		// catch errors within the 'try' statement
		alert(err.message);
		//  console.log(err.message);
	}
	finally {
		// continue execution of code regardless of success failure
		alert('Intensity updates will shoe new alert');
	}
}

// ===========================ACTIVITY=====================================
// activity 1
function ageRangeClassifier(age){


	if (age == 0) {
		return 'you are a baby';
	}else if (age <= 3) {
		return 'you are a toddler';
	}else if (age <= 5) {
		return 'you are a pre-school';
	}else if (age <= 11) {
		return 'you are a schooling age';
	}else if (age <= 19) {
		return 'you are an adolescent';
	}else if (age <= 24) {
		return 'you are a young adult';
	}else if (age <= 64) {
		return 'you are an adult';
	}else if (age >= 65) {
		return 'you are a Senior citizen';
	}else {
		return age + 'is out of bounds';
	}
};


// activity 2
function mathOperation(num1, num2, operator) {
	switch (operator) {
		case '+':
			return num1 + num2;
			break;
		case '-':
			return num1 - num2;
			break;
		case '*':
			return num1 * num2;
			break;
		case '/':
			return num1 / num2;
			break;
		default:
			return operator + ' is out of bounds';
			break;
	}
}